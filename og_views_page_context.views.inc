<?php
/**
 * @file
 * Provides support for the Views module.
 */

/**
 * Implements hook_views_plugins().
 */
function og_views_page_context_views_plugins() {
  $plugins = array();

  $plugins['display_extender']['og_views_page_context'] = array(
    'title' => t('OG Views page context'),
    'handler' => 'views_display_extender_og_views_page_context',
  );

  return $plugins;
}
