<?php
/**
 * @file
 * Definition of views_display_extender_og_views_page_context plugin.
 */

/**
 * A display extender that allows to set the og context through view arguments.
 */
class views_display_extender_og_views_page_context extends views_plugin_display_extender {

  /**
   * {@inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['og_views_page_context'] = array(
      'contains' => array(
        'enabled' => array('default' => FALSE, 'bool' => TRUE),
        'argument' => array('default' => "1"),
        'entity_type' => array('default' => 'node'),
      ),
    );

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  function options_definition_alter(&$options) {
    $options['og_views_page_context'] = array(
      'contains' => array(
        'enabled' => array('default' => FALSE, 'bool' => TRUE),
        'argument' => array('default' => "1"),
        'entity_type' => array('default' => 'node'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  function options_form(&$form, &$form_state) {
    if (!$this->display->has_path() || $form_state['section'] !== 'og_views_page_context') {
      return;
    }

    parent::options_form($form, $form_state);

    // Default options are localed in the "options" property. It seems that they
    // are not applied to the default display options.
    $options = ($this->display->get_option('og_views_page_context') ?: array()) + $this->options['og_views_page_context'];

    // Set a warning message if the related context negotiation is not enabled.
    if (!array_key_exists('og_views_page_context', variable_get('og_context_negotiation_group_context', array()))) {
      $form['warning'] = array(
        '#markup' => t('The following settings require the "OG Views page context" negotiation to be enabled. You can enable it <a href="@url">in the related page</a>.', array(
          '@url' => url('admin/config/group/context'),
        )),
        '#prefix' => '<div class="messages warning">',
        '#suffix' => '</div>',
      );
    }

    $form['og_views_page_context'] = array(
      '#tree' => TRUE,
    );

    $form['og_views_page_context']['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable setting the group context through an entity loaded from path arguments.'),
      '#description' => t('The entity can be a group or a group content.'),
      '#default_value' => $options['enabled'],
    );

    $states = array(
      'visible' => array(
        'input[name="og_views_page_context[enabled]"]' => array('checked' => TRUE),
      ),
    );

    $form['og_views_page_context']['argument'] = array(
      '#type' => 'select',
      '#title' => t('Position of the entity ID argument in path'),
      '#description' => t('Specify in which position of the path the entity ID is found. For example in <em>node/%/page</em>, specify 1.'),
      '#options' => drupal_map_assoc(range(0, 20)),
      '#default_value' => $options['argument'],
      '#states' => $states,
    );

    $form['og_views_page_context']['entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Entity type'),
      '#description' => t('The type of the entity that can be loaded through the path argument specified. Only group and group content entity types can be selected.'),
      '#options' => og_get_all_group_entity() + og_get_all_group_content_entity(),
      '#default_value' => $options['entity_type'],
      '#states' => $states,
    );
  }

  /**
   * {@inheritdoc}
   */
  function options_submit(&$form, &$form_state) {
    if ($form_state['section'] !== 'og_views_page_context') {
      return;
    }

    $this->display->set_option('og_views_page_context', $form_state['values']['og_views_page_context']);
  }

  /**
   * {@inheritdoc}
   */
  function options_summary(&$categories, &$options) {
    parent::options_summary($categories, $options);

    if (!$this->display->has_path()) {
      return;
    }

    $extender_options = $this->display->get_option('og_views_page_context');
    if (!$extender_options['enabled']) {
      $value = t('Not enabled');
    }
    else {
      $value = t('Determine from path argument at position %pos, entity type %type.', array(
        '%pos' => $extender_options['argument'],
        '%type' => $extender_options['entity_type'],
      ));
    }

    $options['og_views_page_context'] = array(
      'category' => 'access',
      'title' => t('Group context'),
      'value' => $value,
    );
  }

}
